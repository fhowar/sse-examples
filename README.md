# SSE Examples

This repository contains a small demo application that I use for 
the Software Systems Engineering class in WS 2015 / 2016 at 
TU Clausthal.

The demo project is used to demonstrate different approaches for
software quality assurance.

### TEL Component ###

The example is derived from an actual hands-free system for 
phones.

### Tool / Method Demos ###

* Code Coverage (JaCoCo)
* Software Model Checking (JPF)
* Static Analysis (PMD)
* Test Case Generation (JDART)
* Model-based Testing (ModBat)

### Instructions ###

I will add instructions and run scripts asap
package de.tuc.ifi.sse.sse.handsfree.ecusw.tel

import modbat.dsl._
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntry;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;

class TelDataModel extends Model {
  var sut: TelDataModule = _

  // transitions
  "reset" -> "zero" := {
    sut = new TelDataModule()
  }
  "zero" -> "init" := {
    sut.initTpbList()
  }
  "zero" -> "end" := {
    sut.getNumberOfEntries()
  } throws("NullPointerException")
  
  "init" -> "one" := {
    val entry = new TelTpbEntry(0, "A. A.", "123")
    sut.setEntry(entry)
  }
  "one" -> "two" := {
    val entry = new TelTpbEntry(0, "A. A.", "123")
    sut.setEntry(entry)
  }
  "two" -> "three" := {
    val entry = new TelTpbEntry(0, "A. A.", "123")
    sut.setEntry(entry)
  }
  "three" -> "end" := {
    val entry = new TelTpbEntry(0, "A. A.", "123")
    sut.setEntry(entry)
  } throws("TelException")
}

package de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data;

/**
 *
 */
public enum TelError {
    FULL_BUFFER, NO_ENTRY;
}

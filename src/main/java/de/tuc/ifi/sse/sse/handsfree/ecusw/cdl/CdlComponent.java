package de.tuc.ifi.sse.sse.handsfree.ecusw.cdl;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api.TelInterfaceForCdl;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntry;


/**
 *
 */
public class CdlComponent implements Runnable {
    
    private final TelInterfaceForCdl tel;

    public CdlComponent(TelInterfaceForCdl tel) {
        this.tel = tel;
    }

    @Override
    public void run() {
        try {

            int id = 0;
            tel.addEntry(new TelTpbEntry(id++, "A. A.", "123"));
            tel.addEntry(new TelTpbEntry(id++, "F. F.", "123"));
            tel.addEntry(new TelTpbEntry(id++, "B. B.", "123"));
            tel.addEntry(new TelTpbEntry(id++, "E. E.", "123"));
            tel.addEntry(new TelTpbEntry(id++, "C. C.", "123"));
            
        } catch (TelException ex) {
            // die silently
        }
    }
    
    
}

package de.tuc.ifi.sse.sse.handsfree.ecusw.can;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api.TelInterfaceForCan;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntryCan;
import java.util.List;

/**
 *
 */
public class CanComponent implements Runnable {
    
    private final TelInterfaceForCan tel;

    public CanComponent(TelInterfaceForCan tel) {
        this.tel = tel;
    }

    @Override
    public void run() {
        
        // 
        // Sleeping for 10 ms should be sufficient for complete initialization
        // of Cdl and Tel ...
        //
        try {
            Thread.sleep(10);
        } catch (InterruptedException ex) {
            // do nothing
        }
        
        try {
            List<TelTpbEntryCan> entries = tel.getAllEntries();            
            for (TelTpbEntryCan ce : entries) {
                System.out.println("Entry: " + ce);
            }
                
        } catch (TelException ex) {
            throw new RuntimeException("TelException: " + ex.getErrorType());
        }
    }
    
    
}

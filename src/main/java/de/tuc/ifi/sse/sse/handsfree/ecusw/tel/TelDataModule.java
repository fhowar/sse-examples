package de.tuc.ifi.sse.sse.handsfree.ecusw.tel;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelError;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntry;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntryCan;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
class TelDataModule {
        
    /**
     * 
     */
    public final int CAPACITY = 5;
 
    //
    // Static Code Analysis Example 1:
    // Though entries may never be initialized, the static analysis 
    // cannot detect this.
    //    
    /**
     * 
     */
    private TelTpbEntry[] entries;
    
    /**
     * 
     */
    private int bufferSize = 0; 
    
        
    /**
     * 
     */
    void initTpbList() {        
        //
        // Static Code Analysis Example 2:
        // When "final" is removed, list is marked as "may not be initialized" 
        //
        final boolean _true = true;
        TelTpbEntry[] buffer;
        if (_true) {
            buffer = new TelTpbEntry[CAPACITY];
        }
        
        this.entries = buffer;
    }
    
    /**
     * 
     * @param entry
     * @throws TelException 
     */
    void setEntry(TelTpbEntry entry) throws TelException {
        if (bufferSize >= CAPACITY) {
            throw new TelException(TelError.FULL_BUFFER);
        }
        
        entries[bufferSize] = entry;
        bufferSize++;
    }
    
    //
    // Symbolic Execution Example:
    // Jdart generates test cases for this method
    //
    /**
     * 
     * @param startEntry
     * @param count
     * @return
     * @throws TelException 
     */
    List<TelTpbEntryCan> getEntries(
            int startEntry, int count) throws TelException {
        
        //
        // Code Coverage Example: 
        // Branch vs. MC/DC vs. Path Coverage
        //
        if (startEntry < 0 || count <= 0 || startEntry + count > bufferSize) {
            throw new TelException(TelError.NO_ENTRY);
        }
        
        List<TelTpbEntryCan> ret = new ArrayList();
        
        for (int i=0; i < count; i++) {
            TelTpbEntry e = this.entries[startEntry + i];
            TelTpbEntryCan ce = new TelTpbEntryCan(i, e.getName() + ", " + e.getNumber());
            ret.add(ce);
        }
        
        return ret;
    }
    
    int getNumberOfEntries() {
        return bufferSize;
    }
    
    void sortLists() {        
        int idx = bufferSize - 1;        
        while (idx > 0) {
            String vJ = entries[idx].getName();
            String vI = entries[idx-1].getName();
            
            if (vJ.compareTo(vI) >= 0) {
                return;
            }
            
            swap(idx, idx-1);
            idx--;           
        }
    }
    
    private void swap(int idx1, int idx2) {
        TelTpbEntry e1 = entries[idx1];
        TelTpbEntry e2 = entries[idx2];
        
        bufferSize--;
        
        entries[idx1] = e2;
        entries[idx2] = e1;        
        
        bufferSize++;
    }
    
}

package de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data;

/**
 *
 */
public class TelTpbEntryCan {
    
    private final int idx;
    
    private final String data;

    public TelTpbEntryCan(int idx, String data) {
        this.idx = idx;
        this.data = data;
    }

    public int getIdx() {
        return idx;
    }

    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        return data;
    }
        
    
}

package de.tuc.ifi.sse.sse.handsfree.ecusw.tel;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api.TelInterfaceForCdl;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api.TelInterfaceForCan;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntry;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntryCan;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * 
 * Non-functional requirements:
 * 
 * 1.1: Sorting entries must be done in less than 450 ms
 * 
 * @author falk
 */
class TelApiModule implements TelInterfaceForCan, TelInterfaceForCdl {
    
    /**
     * data component
     */
    private final TelDataModule data;

    /**
     * 
     * @param data 
     */
    TelApiModule(TelDataModule data) {
        this.data = data;
    }
       
    /**
     * 
     */
    void init() {
        this.data.initTpbList();
    }
    
    /**
     * 
     * 
     * @return
     * @throws TelException 
     */
    @Override
    public List<TelTpbEntryCan> getAllEntries() throws TelException {
        
        int startId = 0;
        int count = this.data.getNumberOfEntries();
        
        // Model checking example 1:
        //
        // if (count < 1) {
        //     return new ArrayList<>();
        // }
        
        return this.data.getEntries(startId, count);
    }

    /**
     * 
     * @param e
     * @throws TelException 
     */
    @Override
    public void addEntry(TelTpbEntry e) throws TelException {
        this.data.setEntry(e);
        this.data.sortLists();
    }

    @Override
    public void addEntries(List<TelTpbEntry> entries) throws TelException {
        for (TelTpbEntry e : entries) {
            this.data.setEntry(e);            
        }

        this.data.sortLists();
    }
    
}

package de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data;

/**
 *
 */
public class TelTpbEntry {
 
    private final int id;
    
    private final String name;
    
    private final String number;

    public TelTpbEntry(int id, String name, String number) {
        this.id = id;
        this.name = name;
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public int getId() {
        return id;
    }
        
}

package de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntry;
import java.util.List;

/**
 *
 * @author falk
 */
public interface TelInterfaceForCdl {

    /**
     * 
     * @param e
     * @throws TelException 
     */
    public void addEntry(TelTpbEntry e) throws TelException;
    
    /**
     * 
     * @param entries
     * @throws TelException 
     */
    public void addEntries(List<TelTpbEntry> entries) throws TelException;
    
}

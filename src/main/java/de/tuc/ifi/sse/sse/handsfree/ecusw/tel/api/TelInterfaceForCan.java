package de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntryCan;
import java.util.List;

/**
 *
 * @author falk
 */
public interface TelInterfaceForCan {


    public List<TelTpbEntryCan> getAllEntries() throws TelException;
    
}

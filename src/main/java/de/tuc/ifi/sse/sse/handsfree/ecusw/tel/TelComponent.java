package de.tuc.ifi.sse.sse.handsfree.ecusw.tel;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api.TelInterfaceForCan;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.api.TelInterfaceForCdl;

/**
 *
 */
public class TelComponent {

    private final TelApiModule api;
    
    private final TelDataModule data;

    public TelComponent() {
        this.data = new TelDataModule();
        this.api = new TelApiModule(data);
        
        api.init();
    }
    
    public TelInterfaceForCan getCanInterface() {
        return api;
    }
    
    public TelInterfaceForCdl getCdlInterface() {
        return api;
    }
    
}

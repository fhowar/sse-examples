package de.tuc.ifi.sse.sse.handsfree.ecusw;

import de.tuc.ifi.sse.sse.handsfree.ecusw.can.CanComponent;
import de.tuc.ifi.sse.sse.handsfree.ecusw.cdl.CdlComponent;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.TelComponent;

/**
 *
 */
public class ECU {
    
    /**
     * 
     */
    private final CanComponent can;
    
    /**
     * 
     */
    private final CdlComponent cdl;
    
    /**
     * 
     */
    private final TelComponent tel;

    /**
     * 
     */
    public ECU() {
        this.tel = new TelComponent();
        this.can = new CanComponent(tel.getCanInterface());
        this.cdl = new CdlComponent(tel.getCdlInterface());
    }

    /**
     * 
     */
    public void start() {
        Thread t1 = new Thread(can);        
        Thread t2 = new Thread(cdl);
    
        t2.start();
        t1.start();          
    }
    
    /**
     * 
     * @param args 
     */
    public static void main(String[] args) {
        ECU ecu = new ECU();        
        ecu.start();
    }
}

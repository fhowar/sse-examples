package de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data;

/**
 *
 */
public class TelException extends Exception {
    
    /**
     * 
     */
    private final TelError errorType;

    /**
     * 
     * @param errorType 
     */
    public TelException(TelError errorType) {
        this.errorType = errorType;
    }

    /**
     * 
     * @return 
     */
    public TelError getErrorType() {
        return errorType;
    }
        
}

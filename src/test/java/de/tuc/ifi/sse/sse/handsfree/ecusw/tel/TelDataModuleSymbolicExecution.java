/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package de.tuc.ifi.sse.sse.handsfree.ecusw.tel;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntry;

/**
 *
 * @author falk
 */
public class TelDataModuleSymbolicExecution {
    
    private final TelDataModule sut = new TelDataModule();

    public TelDataModuleSymbolicExecution() throws TelException {
        sut.initTpbList();
        
        TelTpbEntry entryA = new TelTpbEntry(0, "A. A.", "123");    
        TelTpbEntry entryC = new TelTpbEntry(2, "C. C.", "777");
    
        sut.setEntry(entryA);
        sut.setEntry(entryC);
        
        sut.sortLists();    }

    public void getEntries(int s, int c) throws TelException {
        sut.getEntries(s, c);
    }
    
    public static void main(String[] args) throws TelException {
        
        TelDataModuleSymbolicExecution se = new TelDataModuleSymbolicExecution();
        se.getEntries(0, 2);
    }
}

package de.tuc.ifi.sse.sse.handsfree.ecusw.tel;

import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelException;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntry;
import de.tuc.ifi.sse.sse.handsfree.ecusw.tel.data.TelTpbEntryCan;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 */
public class TelDataModuleTest {
    
    private TelDataModule sut = null;
    
    private final TelTpbEntry entryA = new TelTpbEntry(0, "A. A.", "123");
    
    private final TelTpbEntry entryB = new TelTpbEntry(1, "B. B.", "456");
    
    private final TelTpbEntry entryC = new TelTpbEntry(2, "C. C.", "777");
    
    public TelDataModuleTest() {
    }
    
    @Before
    public void setUp() throws TelException {
        sut = new TelDataModule();
        sut.initTpbList();
        
        sut.setEntry(entryA);
        sut.setEntry(entryC);
        
        sut.sortLists();
    }

    /**
     * Test of setEntry method, of class TelDataModule.
     */
    @Test
    public void testSetEntry() throws Exception {
        System.out.println("setEntry");

        
        sut.setEntry( entryB );

    }

    /**
     * Test of getEntries method, of class TelDataModule.
     */
    @Test
    public void testGetEntries() throws Exception {
        System.out.println("getEntries");
        
        List<TelTpbEntryCan> result = sut.getEntries(0, 2);
        assertEquals(2, result.size());
    }

    /**
     * Test of sortLists method, of class TelDataModule.
     */
    @Test
    public void testSortLists() {
        System.out.println("sortLists");
        // implement test
    }
    
}
